<?php

namespace App\Http\Controllers\Bot\AntiSpam;


use App\Http\Controllers\Bot\Messages\BotEditedMessageController;
use App\Http\Controllers\Bot\Messages\BotMessageController;
use App\Http\Controllers\Bot\Tools;
use App\Http\Controllers\Bot\User\UserController;
use App\Http\Controllers\Controller;
use Cache;
use unreal4u\TelegramAPI\Telegram\Types\Chat;
use unreal4u\TelegramAPI\Telegram\Types\Message;

/**
 * Класс антиспама, проверят сообщения на спам
 * Class AntiSpamController
 * @package App\Http\Controllers\Bot\AntiSpamController
 */
class AntiSpamController extends Controller
{
    /**
     * Время ограничения показа /help
     */
    const HELP_LIMIT_TIME = 1;


    /** @var  string команда, после парсера, что бы был текст и команад отдельно*/
    private $command;
    /** @var  Tools */
    private $tools;
    /** @var  BotMessageController */
    private $botMessageController;


    /**
     * AntiSpamController constructor.
     * @param BotMessageController $botMessage
     */
    public function __construct(BotMessageController $botMessage)
    {
        $this->setBotMessageController($botMessage);
        $this->setTools(Tools::getInstance());
        $this->setCommand($this->getTools()->getCommandFromMessageText($this->getBotMessageController()->getMessage()->text));
    }

    /**
     * Основной метод для запуска
     * Возвращает true - если все хорошо, и false - если был спам
     * @return bool
     */
    public function run()
    {

        // TODO оюъединить все команды в один if
        // Проверяем help
        if (!$this->checkHelp())
        {
            return false;
        }
        // Проверяем /thx
        if (!$this->checkThx()) {
            return false;
        }

        // проверяем /punish
        if (!$this->checkPunish()) {
            return false;
        }


        return true;
    }


    /**
     * Проверяем команду /help на спам
     * Логика такая, что нельзя чаще чем раз в 1 минуту (HELP_LIMIT_TIME) запрашивать /help
     * @return bool если вернем true - значит проверка пройдена и все ок
     */
    private function checkHelp()
    {
        if ($this->getCommand() != BotMessageController::HELP_CMD) {
            // Т.к. эта команда другая - то говорим что ок
            return true;
        }
        $userChatId = $this->getBotMessageController()->getChat()->id;
        $cacheId = sha1(self::class . '_command_' . BotMessageController::HELP_CMD . $userChatId);
        $cacheIdAnswered = sha1(self::class . '_command_' . BotMessageController::HELP_CMD . '_answered' . $userChatId);
        $cacheTime = self::HELP_LIMIT_TIME;

        // Если есть в кеше, значит прошло еще мало времени
        if (Cache::has($cacheId)) {
            // Если мы уже отвечали, то игнорим, если нет - отвечаем что прошло мало времени
            if (!Cache::has($cacheIdAnswered)) {
                // Если нет, то ответим и пометим тот факт, что отвечали
                Cache::add($cacheIdAnswered, true, $cacheTime);

                $sendMessage = $this->getTools()->createSendMessage(
                    $this->getBotMessageController()->getMessage()->message_id,
                    $this->getBotMessageController()->getChat()->id
                );
                $sendMessage->text = "Помощь была уже запрошена менее {$cacheTime} минут(ы) назад";

                $this->getBotMessageController()->getTgLog()->performApiRequest($sendMessage);
            }
            // В любом случае дальше ничего не делаем
            return false;
        }

        // Очистим тот факт, что был ответ, т.к. это уже не важно
        Cache::forget($cacheIdAnswered);
        // Сохраним в кеш факт ответа
        Cache::add($cacheId, true, $cacheTime);

        return true;
    }

    /**
     * Проверяем благодарность нельзя чаще 1 раза в минуту для конкретного человека в конкретном чате
     * @return bool
     */
    private function checkThx()
    {
        if ($this->getCommand() != BotMessageController::THANKS_CMD) {
            // Т.к. эта команда другая - то говорим что ок
            return true;
        }

        $userId = $this->getBotMessageController()->getFromUserController()->getUser()->id;
        $userChatId = $this->getBotMessageController()->getChat()->id;
        $cacheId = sha1(self::class . '_command_' . BotMessageController::THANKS_CMD . $userId . $userChatId);
        $cacheIdAnswered = sha1(self::class . '_command_' . BotMessageController::THANKS_CMD . '_answered' . $userId . $userChatId);
        $cacheTime = self::HELP_LIMIT_TIME;

        // Если есть в кеше, значит прошло еще мало времени
        if (Cache::has($cacheId)) {
            // Если мы уже отвечали, то игнорим, если нет - отвечаем что прошло мало времени
            if (!Cache::has($cacheIdAnswered)) {
                // Если нет, то ответим и пометим тот факт, что отвечали
                Cache::add($cacheIdAnswered, true, $cacheTime);

                $sendMessage = $this->getTools()->createSendMessage(
                    $this->getBotMessageController()->getMessage()->message_id,
                    $this->getBotMessageController()->getChat()->id
                );
                $sendMessage->text = "Вы уже благодарили кого-то менее {$cacheTime} минут(ы) назад";

                $this->getBotMessageController()->getTgLog()->performApiRequest($sendMessage);
            }
            // В любом случае дальше ничего не делаем
            return false;
        }

        // Очистим тот факт, что был ответ, т.к. это уже не важно
        Cache::forget($cacheIdAnswered);
        // Сохраним в кеш факт ответа
        Cache::add($cacheId, true, $cacheTime);

        return true;
    }

    /**
     * Проверяем благодарность нельзя чаще 1 раза в минуту для конкретного человека в конкретном чате
     * @return bool
     */
    private function checkPunish()
    {
        if ($this->getCommand() != BotMessageController::PUNISH_CMD) {
            // Т.к. эта команда другая - то говорим что ок
            return true;
        }
        $userId = $this->getBotMessageController()->getFromUserController()->getUser()->id;
        $userChatId = $this->getBotMessageController()->getChat()->id;
        $cacheId = sha1(self::class . '_command_' . BotMessageController::PUNISH_CMD . $userId . $userChatId);
        $cacheIdAnswered = sha1(self::class . '_command_' . BotMessageController::PUNISH_CMD . '_answered' . $userId . $userChatId);
        $cacheTime = self::HELP_LIMIT_TIME;

        // Если есть в кеше, значит прошло еще мало времени
        if (Cache::has($cacheId)) {
            // Если мы уже отвечали, то игнорим, если нет - отвечаем что прошло мало времени
            if (!Cache::has($cacheIdAnswered)) {
                // Если нет, то ответим и пометим тот факт, что отвечали
                Cache::add($cacheIdAnswered, true, $cacheTime);

                $sendMessage = $this->getTools()->createSendMessage(
                    $this->getBotMessageController()->getMessage()->message_id,
                    $this->getBotMessageController()->getChat()->id
                );
                $sendMessage->text = "Вы уже наказывали кого-то менее {$cacheTime} минут(ы) назад";

                $this->getBotMessageController()->getTgLog()->performApiRequest($sendMessage);
            }
            // В любом случае дальше ничего не делаем
            return false;
        }

        // Очистим тот факт, что был ответ, т.к. это уже не важно
        Cache::forget($cacheIdAnswered);
        // Сохраним в кеш факт ответа
        Cache::add($cacheId, true, $cacheTime);

        return true;
    }


    /**
     * @return string
     */
    protected function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @param string $command
     */
    protected function setCommand(string $command)
    {
        $this->command = $command;
    }

    /**
     * @return Tools
     */
    protected function getTools(): Tools
    {
        return $this->tools;
    }

    /**
     * @param Tools $tools
     */
    protected function setTools(Tools $tools)
    {
        $this->tools = $tools;
    }

    /**
     * @return BotMessageController
     */
    protected function getBotMessageController(): BotMessageController
    {
        return $this->botMessageController;
    }

    /**
     * @param BotMessageController $botMessageController
     */
    protected function setBotMessageController(BotMessageController $botMessageController)
    {
        $this->botMessageController = $botMessageController;
    }

}