<?php

namespace App\Http\Controllers\Bot;


use App\Http\Controllers\Controller;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;

/**
 * Вспомогательный клас, куда можно вынести независящие ни от чего команды
 * Что бы ими можно было пользоватся везде
 * Является SingleTone
 * Class Tools
 * @package app\Http\Controllers\Bot
 */
class Tools extends Controller
{

    /** @var  Tools */
    private static $instance;

    /**
     * Tools constructor.
     */
    private function __construct()
    {
    }

    /**
     * SingleTone
     * @return Tools
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }





    /**
     * Позволяет получить именно команду т.е. /command остальное отсеится
     * @param $messageText
     * @return string
     */
    public function getCommandFromMessageText(string $messageText) : string
    {
        $pattern = '/^(?P<command>\/[a-z]+).*/';
        preg_match($pattern, $messageText, $matches);

        if (!empty($matches)) {
            return $matches['command'];
        } else {
            return '';
        }
    }


    /**
     * Получаем запрос без команды, т.е. список её параметров "/showtop 10" - получим - "10"
     * @param string $cmd
     * @param string $text
     * @return string
     */
    public function getQueryWithoutCmd(string $cmd, string $text)
    {
        return trim(str_replace($cmd, '', $text));
    }



    /**
     * Создает базовую версию SendMessage
     * @param int $messageId
     * @param string $chatId
     * @return SendMessage
     */
    public function createSendMessage(int $messageId, string $chatId)
    {
        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $chatId;
        $sendMessage->parse_mode = 'HTML';
        $sendMessage->reply_to_message_id = $messageId;

        return $sendMessage;
    }

}