<?php

namespace App\Http\Controllers\Bot\User;


use App\Http\Controllers\Bot\Messages\BotCallbackQueryController;
use App\Http\Controllers\Bot\Messages\BotDataController;
use App\Http\Controllers\Bot\Messages\BotEditedMessageController;
use App\Http\Controllers\Bot\Messages\BotMessageController;
use App\Http\Controllers\Controller;
use App\UserMessage;

class UserMessagesController extends Controller
{

    public function __construct()
    {
    }


    /**
     * Сохраняем сообщение от пользователя в БД.
     * Принимаем любой тип унаследованный от основного типа BotDataController
     * @param BotDataController $dataObject
     */
    public function addMessage(BotDataController $dataObject)
    {
        $userMessageModel = new UserMessage();

        $userMessageModel->chat_id = $dataObject->getChat()->id;
        $userMessageModel->telegram_user_id = $dataObject->getFromUserController()->getUser()->telegram_id;
        $userMessageModel->message_type = $dataObject->getType();
        $userMessageModel->text = $dataObject->getDataText();
        $userMessageModel->message_id = $dataObject->getDataId();

        $userMessageModel->save();
    }


}