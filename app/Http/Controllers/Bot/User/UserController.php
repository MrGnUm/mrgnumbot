<?php

namespace App\Http\Controllers\Bot\User;


use App\Http\Controllers\Controller;
use App\User;
use Cache;
use unreal4u\TelegramAPI\Telegram\Types\User as TelegramUser;

/**
 * Класс управления пользователм
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /** Вермя кеширования в минутах */
    const CACHE_TIME = 360;

    /** @var  User */
    private $user;
    /** @var  string */
    private $cacheId;
    /**
     * UserController constructor.
     * @param int $userTelegramId
     * @param int $chatId
     * @param TelegramUser $telegramUser
     * @throws \Exception
     */
    public function __construct(int $userTelegramId, int $chatId, TelegramUser $telegramUser = null)
    {
        $this->setCacheId('telegramID_' . $userTelegramId . 'chatID_' . $chatId);
//        \File::append('debug.dbg', PHP_EOL.PHP_EOL);
//        \File::append('debug.dbg', 'telegramUserID = ' . $userTelegramId.PHP_EOL);
//        \File::append('debug.dbg', 'chatId = ' . $chatId.PHP_EOL);
//        \File::append('debug.dbg', 'cacheId = ' . $this->getCacheId().PHP_EOL);
        /** @var User $user */
        // Получаем из кеша данные, если же их нет, то выполняем функцию, получаем юзера и сохарняем в кеш
        $user = Cache::remember($this->getCacheId(), self::CACHE_TIME, function() use ($userTelegramId, $chatId, $telegramUser) {
//            \File::append('debug.dbg', 'Get From DB!' . PHP_EOL);
            // получаем юзера из БД, по его ID из телеграмма
            $user = User::whereTelegramId($userTelegramId)->whereTelegramChatId($chatId)->first();
            // Если его нет - создаем у нас в БД
            if (is_null($user) && !is_null($telegramUser)) {
                $newUser = new User();
                $newUser->first_name = $telegramUser->first_name;
                $newUser->last_name = $telegramUser->last_name;
                $newUser->telegram_login = $telegramUser->username;
                $newUser->telegram_id = $telegramUser->id;
                $newUser->telegram_chat_id = $chatId;
                $newUser->points = 0;
                $newUser->save();
                // Получаем юзера из БД, только что созданного
                $user = User::whereTelegramId($telegramUser->id)->first();
            }

            return $user;
        });

//        \File::append('debug.dbg', 'userBdId = ' . $user->id.PHP_EOL);

        $this->setUser($user);

        // Если по какой-то причине мы не смогли получить юзера, напримре надо было его создать, а $telegramUser не был передан,
        // то мы удаляем из кеша этот ключ, что бы в будущем можно было получить корретный объект.
        if (is_null($user)) {
            Cache::forget($this->getCacheId());
            throw new \Exception("User doesn't find. telegramId = $userTelegramId, chatId = $chatId");
        }

        // Если не пустой объект юзера пришел, то мы быстренько проверим на актуальность
        if (!is_null($telegramUser)) {
            $this->checkAndUpdateUserInfo($telegramUser);
        }
    }


    /**
     * Добавить (увеличить) переданное количество очков
     * @param int $count
     */
    public function incrementPoints(int $count)
    {
        $this->getUser()->points += $count;
//        \File::append('debug.dbg', 'incrementPoints'.PHP_EOL);
//        \File::append('debug.dbg', 'telegramUserID = ' . $this->getUser()->telegram_id.PHP_EOL);
//        \File::append('debug.dbg', 'chatId = ' . $this->getUser()->telegram_chat_id.PHP_EOL);
        $this->getUser()->save();
        $this->removeFromCache();
    }


    /**
     * Отнять (уменьшить) переданное количество очков
     * @param int $count
     */
    public function decrementPoints(int $count)
    {
        $this->getUser()->points -= $count;
        $this->getUser()->save();
        $this->removeFromCache();
    }

    /**
     * Удаляем пользвоателя из Кеша
     */
    private function removeFromCache()
    {
//        \File::append('debug.dbg', 'removeFromCache' . PHP_EOL);
//        \File::append('debug.dbg', 'username = ' . $this->getFullName() . PHP_EOL);
//        \File::append('debug.dbg', 'cacheId = ' . $this->getCacheId().PHP_EOL);
        Cache::forget($this->getCacheId());
    }


    /**
     * Проверяем, если какие-то параметры в telegram изменились, то мы их применяем
     * @param TelegramUser $telegramUser
     */
    private function checkAndUpdateUserInfo(TelegramUser $telegramUser)
    {
        $currentUser = $this->getUser();
        $change = false;

        // Проверяем Имя
        if ($currentUser->first_name != $telegramUser->first_name) {
            $currentUser->first_name = $telegramUser->first_name;
            $change = true;
        }

        // Проверяем фамилию
        if ($currentUser->last_name != $telegramUser->last_name) {
            $currentUser->last_name = $telegramUser->last_name;
            $change = true;
        }

        // Проверяем Логин
        if ($currentUser->telegram_login != $telegramUser->username) {
            $currentUser->telegram_login = $telegramUser->username;
            $change = true;
        }

        // Сохарняем и сбрасываем кеш, если есть изменения
        if ($change) {
            $this->removeFromCache();
            $currentUser->save();
        }
    }


    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    protected function setUser(User $user)
    {
        $this->user = $user;
    }


    /**
     * Возвращает полное имя пользоватея в формте Иванов Иван (@ivanov)
     * @param bool $needAt - если передать false - символа собаки "@" не будет
     * @return string
     */
    public function getFullName($needAt = true)
    {
        $user = $this->getUser();

        $fullName = "{$user->first_name} {$user->last_name} (";
        if ($needAt) {
            $fullName .= "@";
        }
        $fullName .= "{$user->telegram_login})";

        return $fullName;
    }

    /**
     * @return string
     */
    public function getCacheId(): string
    {
        return $this->cacheId;
    }

    /**
     * @param string $cacheId
     */
    public function setCacheId(string $cacheId)
    {
        $this->cacheId = $cacheId;
    }
}
