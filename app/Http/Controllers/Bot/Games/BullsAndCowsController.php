<?php

namespace App\Http\Controllers\Bot\Games;


use App\BullsAndCows;
use App\Http\Controllers\Bot\Messages\BotMessageController;
use App\Http\Controllers\Controller;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;

/**
 * Игра "Быки и Коровы"
 * Основная логика игры и все что с ней связано
 * Class BullsAndCows
 * @package App\Http\Controllers\Bot\Games
 */
class BullsAndCowsController extends Controller
{

    const BULLS_CMD = '/bull';

    const START_PARAMS = 'start';
    const INFO_PARAMS = 'info';
    const HELP_PARAMS = 'help';

    /** Количесто знаков в угадываем числе */
    const SIGN_COUNT = 4;
    const DEFAULT_PRESENT_COUNT = 50;

    /** @var  BullsAndCowsController */
    private static $instance;
    /** @var  BotMessageController */
    private $messageController;
    /** @var  string строка запроса, то, что бзе команды */
    private $query;
    /** @var  BullsAndCows */
    private $currentGame;


    /**
     * BullsAndCows constructor.
     * @param BotMessageController $messageController
     */
    private function __construct(BotMessageController $messageController)
    {
        $this->setMessageController($messageController);
        // Созраняем запрос без команды, в дальнейшем позволит работаь сразу с паарметрами
        $this->setQuery($this->getMessageController()->getTools()->getQueryWithoutCmd(
            self::BULLS_CMD,
            $this->getMessageController()->getMessage()->text
        ));

        // Пытаемся получить текущю игру
        /** @var BullsAndCows $currentGame */
        $currentGame = BullsAndCows::whereChatId($this->getMessageController()->getChat()->id)
            ->whereGameStop(null)
            ->orderBy('id', 'desc')
            ->first();

        if (!is_null($currentGame)) {
            $this->setCurrentGame($currentGame);
        }

    }

    /**
     * SingleTone
     * @param BotMessageController $botMessageController
     * @return BullsAndCowsController
     */
    public static function getInstance(BotMessageController $botMessageController)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($botMessageController);
        }
        return self::$instance;
    }


    /**
     * Основной метод запуска игры "Быки и коровы"
     */
    public function run()
    {
        // Если прислали без параметра - выводим помощь
        if (empty($this->getQuery())) {
            $this->sendHelp();
            return true;
        }

        switch ($this->getQuery()) {
            case self::START_PARAMS :
                $this->start();
                break;
            case self::HELP_PARAMS :
                $this->sendHelp();
                break;
            case self::INFO_PARAMS :
                $this->sendCurrentInfo();
                break;

            default:
                $this->checkAnswer();
                break;

        }

        return true;
    }


    /**
     * Стартуем игру, если уже есть одна ативная игра в этом чате, то сообщаем об этом
     */
    private function start()
    {
        $sendMessage = $this->createSendMessage();

        // Если пусто, значит нет текущей, незавершенной игры, - запускаем новую
        if (is_null($this->getCurrentGame())) {

            // Генерируем новое число
            $newNumber = $this->getRandomNumberStr();
            // Сохраняем новую игру
            $bullsAndCowsModel = new BullsAndCows();
            $bullsAndCowsModel->question_number = $newNumber;
            $bullsAndCowsModel->chat_id = $this->getMessageController()->getChat()->id;
            $bullsAndCowsModel->save();

            $sendMessage->text = 'Игра начата! Удачи!';
        } else {
            $sendMessage->text = "Игра уже идет! Предпринятых попыток: <b>{$this->getCurrentGame()->attempts_count}</b>";
        }

        $this->getMessageController()->getTgLog()->performApiRequest($sendMessage);
    }


    /**
     * Отправялем опсиание игры
     */
    private function sendHelp()
    {
        $sendMessage = $this->createSendMessage();

        $sendMessage->disable_web_page_preview = true;
        $sendMessage->text = 'Игра "Быки и Коровы"' . PHP_EOL;
        $sendMessage->text .= "Подробнее о самой игре можно посмотреть на <a href=\"https://ru.wikipedia.org/wiki/%D0%91%D1%8B%D0%BA%D0%B8_%D0%B8_%D0%BA%D0%BE%D1%80%D0%BE%D0%B2%D1%8B\">wikipedia</a>" . PHP_EOL. PHP_EOL;
        $sendMessage->text .= 'Основные моменты, используемые здесь:' . PHP_EOL;
        $sendMessage->text .= '- Загадываемое число - 4ех значное' . PHP_EOL;
        $sendMessage->text .= '- Числа в нем <b>не повторяются</b>' . PHP_EOL;
        $sendMessage->text .= '- Формат ответа: Б:1, К:2 - 1 бык, и 2 коровы' . PHP_EOL;
        $sendMessage->text .= '- Базовое колическо очков за победу: ' . self::DEFAULT_PRESENT_COUNT . PHP_EOL . PHP_EOL;
        $sendMessage->text .= 'Команды' . PHP_EOL;
        $sendMessage->text .= self::BULLS_CMD . ' ' . self::START_PARAMS . ' - начать игру' . PHP_EOL;
        $sendMessage->text .= self::BULLS_CMD . ' ' . self::INFO_PARAMS . ' - Получить информацию о текущей игре' . PHP_EOL;
        $sendMessage->text .= self::BULLS_CMD . ' ' . self::HELP_PARAMS . ' - Вывести помощь (тоже самое что и /bull )' . PHP_EOL;
        $sendMessage->text .= self::BULLS_CMD . ' 1234 - Попытаться угадать число (число должно быть 4ех значным)' . PHP_EOL;
        $this->getMessageController()->getTgLog()->performApiRequest($sendMessage);

    }


    /**
     * Отпарвялем мформацию о текущей игре
     */
    private function sendCurrentInfo()
    {
        $curGame = $this->getCurrentGame();
        $sendMessage = $this->createSendMessage();

        if (!empty($curGame)) {
            $sendMessage->text = "<code>Текущая игра:</code> №{$curGame->id}" . PHP_EOL;
            $sendMessage->text .= "<code>Было попыток:</code> {$curGame->attempts_count}" . PHP_EOL;
            $sendMessage->text .= "<code>Можно получить:</code> {$this->getWinCount()} очков" . PHP_EOL;
        } else {
            $sendMessage->text = 'Нет текущей игры. Что бы начать напишете <b>' . self::BULLS_CMD . ' ' . self::START_PARAMS . '</b>';
        }

        $this->getMessageController()->getTgLog()->performApiRequest($sendMessage);

    }


    /**
     * Проверяем ответ
     */
    private function checkAnswer()
    {
        $signCount = self::SIGN_COUNT;
        $sendMessage = $this->createSendMessage();

        $curGame = $this->getCurrentGame();
        if (!is_null($curGame)) {
            // Смотрим ровно 4ех значное число (количество знаком задается $signCount)
            $pattern = "/^(?P<answer>[\d]{" . $signCount . "})$/";
            preg_match($pattern, $this->getQuery(), $matches);
            if (!empty($matches)) {
                if ($matches['answer'] == $curGame->question_number) {
                    $winCount = $this->getWinCount();
                    $this->getMessageController()->getFromUserController()->incrementPoints($winCount);
                    $this->stopGame($this->getMessageController()->getFromUserController()->getUser()->telegram_id);
                    $sendMessage->text = "Поздравляю! Ваша награда: <b>{$winCount} очков</b>. Всего: <b>{$this->getMessageController()->getFromUserController()->getUser()->points}</b>";
                } else {
                    $result = $this->findBullsAndCows($matches['answer']);
                    $sendMessage->text = "Б:{$result['bulls']}, К:{$result['cows']}";
                    $this->addAttemptToGame();
                }
            } else {
                $sendMessage->text = "Неизвестный параметр \"<b>{$this->getQuery()}\"</b>";
            }
        } else {
            $sendMessage->text = 'Нет текущей игры. Что бы начать напишете <b>' . self::BULLS_CMD . ' ' . self::START_PARAMS . '</b>';
        }

        $this->getMessageController()->getTgLog()->performApiRequest($sendMessage);
    }


    /**
     * Считаем количество быков и коров
     * @param string $answer
     * @return array
     */
    protected function findBullsAndCows(string $answer){

        // разбиваем строку на массив
        $gameNumberArray = preg_split('//u', $this->getCurrentGame()->question_number, -1, PREG_SPLIT_NO_EMPTY);
        // разбиваем строку на массив
        $answerArray = preg_split('//u', $answer, -1, PREG_SPLIT_NO_EMPTY);


        $bullsCount = 0;
        foreach ($gameNumberArray as $key => $number) {
            if ($number == $answerArray[$key]) {
                $bullsCount++;
                unset($gameNumberArray[$key]);
            }
        }

        $cowCount = count(array_intersect($gameNumberArray, $answerArray));
        return array('bulls' => $bullsCount, 'cows' => $cowCount);
    }


    /**
     * Увеличиваем на 1 количество попыток
     */
    private function addAttemptToGame()
    {
        $curGame = $this->getCurrentGame();
        $curGame->attempts_count++;
        $curGame->save();
    }


    /**
     * останавливаем текущую игру
     * @param string $winnerId
     */
    private function stopGame(string $winnerId = '')
    {
        $curGame = $this->getCurrentGame();
        $curGame->game_stop = true;
        if (!empty($winnerId)) {
            $curGame->winner_telegram_id = $winnerId;
        }
        $curGame->save();
    }


    /**
     * Возвращает количество очков, котоыре можно заработать за текущую игру
     * @return integer
     */
    private function getWinCount()
    {
        // TODO Сделать уменьшение количеста от кол-ва попыток
        $count = self::DEFAULT_PRESENT_COUNT;
        return $count;
    }


    /**
     * Генеируем случайное число, заданной длинны (разрядности) и при этом, что бы числа в разрядах неповторялись
     * Рекурсивная функция
     * @param int $count - количество разрядов в числе
     * @param array $current - текущий массив собранного числа, может быть задан по умолчанию
     * @return string
     */
    protected function getRandomNumberStr(int $count = self::SIGN_COUNT, array $current = array())
    {

        // Если мы набрали нужное количесво, возвращаем готовое число
        if (count($current) == $count) {
            return implode('', $current);
        }

        $newNumber = mt_rand(0,9); // создаем произвольное число от 0 до 9, т.к. мы в десятичной системе =)

        // Если в массиве нет такого числа, то добавялем его
        if (array_search($newNumber, $current) === false) {
            $current[] = $newNumber;
        }

        return $this->getRandomNumberStr($count, $current);
    }


    /**
     * @return SendMessage
     */
    private function createSendMessage()
    {
        return $this->getMessageController()->getTools()->createSendMessage(
            $this->getMessageController()->getMessage()->message_id,
            $this->getMessageController()->getChat()->id
        );
    }

    /**
     * @return string
     */
    protected function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    protected function setQuery(string $query)
    {
        $this->query = $query;
    }


    /**
     * @return BotMessageController
     */
    protected function getMessageController(): BotMessageController
    {
        return $this->messageController;
    }

    /**
     * @param BotMessageController $messageController
     */
    protected function setMessageController(BotMessageController $messageController)
    {
        $this->messageController = $messageController;
    }

    /**
     * @return BullsAndCows
     */
    protected function getCurrentGame()
    {
        return $this->currentGame;
    }

    /**
     * @param BullsAndCows $currentGame
     */
    protected function setCurrentGame(BullsAndCows $currentGame)
    {
        $this->currentGame = $currentGame;
    }


}