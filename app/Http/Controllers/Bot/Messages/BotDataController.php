<?php

namespace App\Http\Controllers\Bot\Messages;

use App\Bot;
use App\Http\Controllers\Bot\Tools;
use App\Http\Controllers\Bot\User\UserController;
use App\Http\Controllers\Bot\User\UserMessagesController;
use App\User;
use App\Http\Controllers\Controller;
use unreal4u\TelegramAPI\Telegram\Methods\AnswerCallbackQuery;
use unreal4u\TelegramAPI\Telegram\Methods\EditMessageText;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\Chat;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\Message;
use unreal4u\TelegramAPI\TgLog;


/**
 * Абстрактный класс, который говорит всем классам бота, работающими с данными (message, editedmessage, CallbackQuery и тд)
 * Какие должны быть базовые методы, например метод run
 * А так же сюда вынесли все общие методы TODO возможно их стоит куда то еще вынести
 * Class BotDataController
 * @package app\Http\Controllers
 */
abstract class BotDataController extends Controller
{
    // Типы сообщений
    const SEND_MESSAGE_TYPE = 'SendMessage';
    const EDIT_MESSAGE_TYPE = 'EditMessageText';
    const CALLBACK_QUERY_TYPE = 'CallbackQuery';

    // Количество очков, котоыре снимут за наглость, если попытаться себя побалгодарить
    const DECREMENT_COUNT_FOR_IMPUDENCE = 20;
    // Количество очков, котоыре снимутся за попытку наказать себя
    const DECREMENT_COUNT_FOR_SELF_PUNISH = 5;
    // Максмиальнео количество для вывода /showtop
    const MAX_SHOW_TOP_COUNT = 30;

    /** @var  TgLog основной объект, который работает с API Telegram */
    private $tgLog;
    /** @var  Bot */
    private $bot;
    /** @var  Chat */
    private $chat;
    /** @var  string*/
    private $type;
    /** @var  UserController */
    private $fromUserController;
    /** @var  UserMessagesController */
    private $userMessageController;
    /** @var  Tools вспомогательный класс, содержит полезные методы */
    private $tools;
    /** @var  string Текст пришедшего сообщения, именно то, что пришло  */
    private $dataText;
    /** @var  string */
    private $dataId;

    /**
     * BotDataController constructor.
     * @param TgLog $tgLog
     * @throws \Exception
     */
    public function __construct(TgLog $tgLog)
    {
        if (empty(env('TELEGRAM_BOT_TOKEN'))) {
            throw new \Exception('Telegram Bot Token is empty!');
        }
        // Сохраняем главный объект для работы с API Telegram
        $this->setTgLog($tgLog);
        $this->setMyType();
        $this->setUserMessageController(new UserMessagesController());
        $this->setTools(Tools::getInstance());
    }


    abstract public function run();

    /**
     * Задать свой тип
     * @return mixed
     */
    abstract protected function setMyType();

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDataText(): string
    {
        return $this->dataText;
    }

    /**
     * @param string $dataText
     */
    public function setDataText(string $dataText)
    {
        $this->dataText = $dataText;
    }

    /**
     * @return string
     */
    public function getDataId(): string
    {
        return $this->dataId;
    }

    /**
     * @param string $dataId
     */
    public function setDataId(string $dataId)
    {
        $this->dataId = $dataId;
    }

    /**
     * @return Tools
     */
    public function getTools(): Tools
    {
        return $this->tools;
    }

    /**
     * @param Tools $tools
     */
    public function setTools(Tools $tools)
    {
        $this->tools = $tools;
    }


    /**
     * @return Message
     */
    abstract public function getMessage();


    /**
     * Инициализурем бота из БД, если его нет - создаем
     * @param string $chatId
     */
    public function initBotFromBD(string $chatId)
    {
        $botModel = new Bot();
        $bot = $botModel->whereTelegramChatId($chatId)->first();
        // Если ничего не найдено сохраняем в БД этот чат (по нему происходит нициализация бота)
        if (is_null($bot)) {
            $botModel->telegram_chat_id = $chatId;
            $botModel->save();
            // получаем из БД только что сохраненного бота TODO надо подумать действительно ли это надо? может только ID получить и всё?
            $bot = $botModel->where('telegram_chat_id', $chatId)->first();
        }
        // Сохраняем полученный объект бота из БД
        $this->setBot($bot);
    }

    /**
     * Хелпер, который создает и заполняет базовыми свойствами объект SendMessage
     * @return SendMessage
     */
    protected function createSendMessage()
    {
        return $this->getTools()->createSendMessage($this->getMessage()->message_id, $this->getChat()->id);
    }

    /**
     * Хелпер, который создает и заполняет базовыми свойствами объект EditMessage
     * @return EditMessageText
     */
    protected function createEditedMessage()
    {
        $editMessage = new EditMessageText();
        $editMessage->chat_id = $this->getChat()->id;
        $editMessage->parse_mode = 'HTML';
        $editMessage->message_id = $this->getMessage()->message_id;

        return $editMessage;
    }




    /**
     * Хелпер
     * Пытаемся найти пользвателя по переданной строке, это либо логин вида "@Login"
     * Либо просто строка, и будем искать как по логину, так по имени и по фамилии
     * @param string $str
     * @return User[]
     */
    protected function findUser(string $str)
    {
        $str = trim($str);
        // Если пришло меньеш 2ух символов т.е. пусто или 1 символ - игнорим
        if (intval(mb_strlen($str)) < 2) {
            return array();
        }

        // Пытаеся понять нам прислали логин или нет
        $pattern = '/^@(?P<login>[\w]+)/u';
        preg_match($pattern, $str, $matches);
        // Если это всё таки логин, то будем пытаться найти по логину пользователя
        if (isset($matches['login'])) {
            return $this->findUserByLogin($matches['login']);
        }

        // Если же это не логин, пытаемся найти по Фамили, Имени или Логину как строка
        $pattern = '/^(?P<name>[\w]+)/u';
        preg_match($pattern, $str, $matches);
        if (isset($matches['name'])) {
            return $this->findUserByOther($matches['name']);
        }

        // Если ничего не подошло - вернем пустой массив
        return array();
    }


    /**
     * Хелпер
     * Попытка найти пользователя по телеграм логину, сам логин регуляркой из строки будем пытаться достать
     * @param string $login
     * @return User[]
     */
    protected function findUserByLogin(string $login)
    {

        // Если это всё таки логин, то будем пытаться найти по логину пользователя
        if (!empty($login)) {
            $users = User::select(array('telegram_id'))
                ->whereTelegramLogin($login)
                ->whereTelegramChatId($this->getChat()->id)
                ->get();

            // Если что-то нашли - возвращаем, иначе возвращаем пустой массив
            if ($users->count() > 0) {
                return $users->all();
            }
        }

        return array();
    }


    /**
     * Хелпер
     * Пытаемся найти пользователя по его last_name, first_name и telegram_login
     * @param string $name
     * @return User[]
     */
    protected function findUserByOther(string $name)
    {
        // пытаемся найти по Фамили, Имени или Логину как строка
        if (!empty($name)) {
            $users = User::select(array('telegram_id'))
                ->whereTelegramChatId($this->getChat()->id)
                ->where(function ($query) use ($name){
                    /** @var User $query */
                    $query->where('telegram_login', 'like', "%{$name}%")
                        ->orWhere('last_name', 'like', "%{$name}%")
                        ->orWhere('first_name', 'like', "%{$name}%");
                })->get();

            // Если чтото нашли - возвращаем, иначе возвращаем пустой массив
            if ($users->count() >0) {
                return $users->all();
            }
        }
        return array();
    }


    /**
     * Хелпер
     * Общая функция, которая отправляет сообщение, с подтвеждением о благодарности
     * @param UserController $userFrom
     * @param UserController $userTo
     * @param string $messageType
     * @return bool
     */
    protected function sendConfirmationToThanks(UserController $userFrom, UserController $userTo, string $messageType = BotDataController::SEND_MESSAGE_TYPE)
    {
        $messageToSend = $this->createMessageToSend($messageType);


        // Если пытаются поблагодарить самого себя - то мы об этом напишем и еще вычтем некоторое количество очков! Если все хорошо - то все как обычно =)
        if ($userTo->getUser()->telegram_id !== $userFrom->getUser()->telegram_id) {
            $messageToSend->text = 'Вы действительно хотите поблагодарить ' . $userTo->getFullName() . ' ?';

            $replyUserId = $userTo->getUser()->telegram_id;

            // Получаем команды, имено те, что будет обрабатываеть CallBackQueryController
            $thxCmd = BotCallbackQueryController::THANKS_CMD;
            $markup = new Markup();
            $markup->inline_keyboard = array(
                array(
                    array('text' => 'Нет, не хочу', 'callback_data' => "{$thxCmd}0u$replyUserId"),
                ),
                array(
                    array('text' => '+5', 'callback_data' => "{$thxCmd}5u$replyUserId"),
                    array('text' => '+10', 'callback_data' => "{$thxCmd}10u$replyUserId"),
                    array('text' => '+15', 'callback_data' => "{$thxCmd}15u$replyUserId"),
                )
            );
            $messageToSend->reply_markup = $markup;
        } else {
            // на сколько очков уменьшить количество за наглость =)
            $decrementCount = self::DECREMENT_COUNT_FOR_IMPUDENCE;
            $userFrom->decrementPoints($decrementCount);
            $messageToSend->text = $userFrom->getFullName() . ', это очень некрасиво пытаться накрутить себе очки!' . PHP_EOL;
            $messageToSend->text .= "Поэтому я лишаю тебя <b>$decrementCount очков</b>. Всего: <b>{$userFrom->getUser()->points}</b>";
        }

        $this->getTgLog()->performApiRequest($messageToSend);

        return true;
    }


    /**
     * Хелпер
     * Отправялем запрос с выбором, кого поблагодарить
     * @param array $users
     * @return bool
     */
    protected function SendChooseWhoYouWantToThank(array $users)
    {
        /** @var User[]  $users */

        $sendMessage = $this->createSendMessage();
        $sendMessage->text = 'Выберите, кого хотите поблагодарить:';
        $markup = new Markup();
        $markup->inline_keyboard = array();
        foreach ($users as $user) {
            $userModel = new UserController($user->telegram_id, $this->getChat()->id);

            $cmd = BotCallbackQueryController::THANKS_CMD;
            $btn = array(
                array('text' => $userModel->getFullName(), 'callback_data' => $cmd . ' u'.$userModel->getUser()->telegram_id),
            );
            $markup->inline_keyboard[] = $btn;
        }
        $sendMessage->reply_markup = $markup;
        $this->getTgLog()->performApiRequest($sendMessage);
        return true;
    }


    /**
     * Хелпер
     * Общая функция, отправялем подтверждение о наказании
     * @param UserController $userFrom
     * @param UserController $userTo
     * @param string $messageType
     * @return bool
     */
    protected function sendConfirmationToPunish(UserController $userFrom, UserController $userTo, string $messageType = BotDataController::SEND_MESSAGE_TYPE)
    {

        $messageToSend = $this->createMessageToSend($messageType);


        // Если пытаются наказать самого себя - то мы об этом напишем
        if ($userTo->getUser()->telegram_id !== $userFrom->getUser()->telegram_id) {
            $messageToSend->text = 'Вы действительно хотите наказать ' . $userTo->getFullName() . ' ?';

            $replyUserId = $userTo->getUser()->telegram_id;

            // Получаем команды, имено те, что будет обрабатываеть CallBackQueryController
            $punishCmd = BotCallbackQueryController::PUNISH_CMD;

            $markup = new Markup();
            $markup->inline_keyboard = array(
                array(
                    array('text' => 'Нет, не хочу', 'callback_data' => "{$punishCmd}0u$replyUserId"),
                ),
                array(
                    array('text' => '-5', 'callback_data' => "{$punishCmd}5u$replyUserId"),
                    array('text' => '-10', 'callback_data' => "{$punishCmd}10u$replyUserId"),
                    array('text' => '-15', 'callback_data' => "{$punishCmd}15u$replyUserId"),
                )
            );
            $messageToSend->reply_markup = $markup;
        } else {
            // Пытаешься сам себя наказать?
            $decrementCount = self::DECREMENT_COUNT_FOR_SELF_PUNISH;
            $userFrom->decrementPoints($decrementCount);
            $messageToSend->text = $userFrom->getFullName() . ', пытаешься сам(а) себя наказать?! 😧' . PHP_EOL;
            $messageToSend->text .= "Это похоже на мазохизм! Но так и быть, чуток сниму с тебя очков. Снято <b>$decrementCount очков</b>. Всего: <b>{$userFrom->getUser()->points}</b>";
        }

        $this->getTgLog()->performApiRequest($messageToSend);

        return true;
    }


    /**
     * Хелпер
     * Отправляем запрос с выбором кого наказать
     * @param array $users
     * @return bool
     */
    protected function SendChooseWhoYouWantToPunish(array $users)
    {
        /** @var User[]  $users */

        $sendMessage = $this->createSendMessage();
        $sendMessage->text = 'Выберите, кого хотите наказать:';
        $markup = new Markup();
        $markup->inline_keyboard = array();
        foreach ($users as $user) {
            $userModel = new UserController($user->telegram_id, $this->getChat()->id);

            $cmd = BotCallbackQueryController::PUNISH_CMD;
            $btn = array(
                array('text' => $userModel->getFullName(), 'callback_data' => $cmd . ' u'.$userModel->getUser()->telegram_id),
            );
            $markup->inline_keyboard[] = $btn;
        }
        $sendMessage->reply_markup = $markup;
        $this->getTgLog()->performApiRequest($sendMessage);
        return true;
    }


    /**
     * Посылаем запрос чью инфу показать
     * @param array $users
     * @return bool
     */
    protected function SendChooseWhoseInfoShow(array $users) : bool
    {
        /** @var User[] $users */
        $sendMessage = $this->createSendMessage();
        $sendMessage->text = 'Выберите, чью информацию вам показать:';
        $markup = new Markup();
        $markup->inline_keyboard = array();
        foreach ($users as $user) {
            $userModel = new UserController($user->telegram_id, $this->getChat()->id);

            $cmd = BotCallbackQueryController::INFO_CMD;
            $btn = array(
                array('text' => $userModel->getFullName(), 'callback_data' => $cmd . ' u'.$userModel->getUser()->telegram_id),
            );
            $markup->inline_keyboard[] = $btn;
        }
        $sendMessage->reply_markup = $markup;
        $this->getTgLog()->performApiRequest($sendMessage);
        return true;
    }


    /**
     * Получаем место, которое занимает переданный юзер
     * @param UserController $user
     * @return int
     */
    protected function getUserPlace(UserController $user) : int
    {
        // Получаем у скольки человек количество очков больше чем, у запросившего пользвателя
        $count = User::whereTelegramChatId($this->getChat()->id)->where('points', '>', $user->getUser()->points)->get()->count();
        // Увеличиваем на единицу, т.к. его место следующее =)
        $count++;

        return $count;
    }


    /**
     * Возвращает созданное сообщение для отправки, по переданному типу
     * Если передано что-то отличное от щнакомых типов - возвращаем стандартный тип SendMessage
     * @param string $messageType
     * @return EditMessageText|SendMessage|null
     */
    protected function createMessageToSend(string $messageType)
    {

        switch ($messageType) {
            case BotDataController::SEND_MESSAGE_TYPE :
                return $this->createSendMessage();
                break;
            case BotDataController::EDIT_MESSAGE_TYPE :
                return $this->createEditedMessage();
                break;

            default :
                return null;
                break;
        }
    }


    /**
     * Отправялем информацию о пользователе, тип сообщения зависит от параметров
     * @param UserController $user
     * @param string $messageType
     * @return bool
     */
    protected function sendUserInfo(UserController $user, string $messageType = BotDataController::SEND_MESSAGE_TYPE)
    {

        $messageToSend = $this->createMessageToSend($messageType);

        $messageToSend->text .= '<code>Имя</code>: ' . $user->getUser()->first_name . PHP_EOL;
        $messageToSend->text .= '<code>Фамилия</code>: ' . $user->getUser()->last_name . PHP_EOL;
        $messageToSend->text .= '<code>Логин</code>: ' . $user->getUser()->telegram_login . PHP_EOL;
        $messageToSend->text .= '<code>Очки</code>: ' . $user->getUser()->points . PHP_EOL;
        $messageToSend->text .= '<code>Место</code>: ' . $this->getUserPlace($user) . '-ое';

        $this->getTgLog()->performApiRequest($messageToSend);

        return true;
    }


    /**
     * Показываем алерт либо плажкой, либо как alert всё зависит от параметров
     * @param string $callbackQueryId - id callback запроса, при нажатии на inlineKeyboard
     * @param string $text - Текст, который необходимо отправить, можно без него
     * @param bool $showAlert - показывать в виде alert или же ввиде плажки
     * @return bool
     */
    protected function sendAlert(string $callbackQueryId, string $text = 'Неверная команда', bool $showAlert = false)
    {
        $answerCallBackQuery = new AnswerCallbackQuery();
        $answerCallBackQuery->callback_query_id = $callbackQueryId;
        $answerCallBackQuery->text = $text;
        $answerCallBackQuery->show_alert = $showAlert;


        $this->getTgLog()->performApiRequest($answerCallBackQuery);
        return true;
    }

    /**
     * @return TgLog
     */
    public function getTgLog(): TgLog
    {
        return $this->tgLog;
    }

    /**
     * @param TgLog $tgLog
     */
    public function setTgLog(TgLog $tgLog)
    {
        $this->tgLog = $tgLog;
    }

    /**
     * @return Bot
     */
    public function getBot(): Bot
    {
        return $this->bot;
    }

    /**
     * @param Bot $bot
     */
    public function setBot(Bot $bot)
    {
        $this->bot = $bot;
    }

    /**
     * @return Chat
     */
    public function getChat(): Chat
    {
        return $this->chat;
    }

    /**
     * @param Chat $chat
     */
    public function setChat(Chat $chat)
    {
        $this->chat = $chat;
    }

    /**
     * @return UserController
     */
    public function getFromUserController(): UserController
    {
        return $this->fromUserController;
    }

    /**
     * @param UserController $fromUserController
     */
    public function setFromUserController(UserController $fromUserController)
    {
        $this->fromUserController = $fromUserController;
    }


    /**
     * @return UserMessagesController
     */
    public function getUserMessageController(): UserMessagesController
    {
        return $this->userMessageController;
    }


    /**
     * @param UserMessagesController $userMessageController
     */
    public function setUserMessageController(UserMessagesController $userMessageController)
    {
        $this->userMessageController = $userMessageController;
    }


}