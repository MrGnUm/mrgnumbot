<?php

namespace App\Http\Controllers\Bot\Messages;


use unreal4u\TelegramAPI\Telegram\Methods\AnswerInlineQuery;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Query;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Query\Result\Article;
use unreal4u\TelegramAPI\Telegram\Types\InputMessageContent\Text;
use unreal4u\TelegramAPI\TgLog;

/**
 * Клас для обработки Inline бота. Т.е. то, что пишется в процесе ввода, а не после отправки
 * Class BotInlineQueryController
 * @package App\Http\Controllers
 */
class BotInlineQueryController extends BotDataController
{

    /** @var  Query */
    private $query;


    public function __construct(TgLog $tgLog, Query $query)
    {
        // вызываем первым делом родителя он инициализует основные вещи для дальнейшей работы
        parent::__construct($tgLog);
        // сохраняем $query
        $this->setQuery($query);
//        $this->setFromUserController(new UserController());

    }

    public function getMessage()
    {

    }


    public function setMyType()
    {
        // TODO: Implement setMyType() method.
    }

    public function run()
    {
        $queryText = $this->getQuery()->query;

        switch ($queryText) {
            case '/thx':
                $this->test();
                break;
        }
    }

    /**
     * @return Query
     */
    public function getQuery(): Query
    {
        return $this->query;
    }

    /**
     * @param Query $query
     */
    public function setQuery(Query $query)
    {
        $this->query = $query;
    }

    private function test()
    {


        $text = new Text();
        $text->message_text = '/showtop15';

        $markup = new Markup();
        $markup->inline_keyboard = array(
            array(
                array('text' => 'Нет, не хочу', 'callback_data' => "no"),
                array('text' => 'Уберись!', 'callback_data' => "go away"),
            ),
            array(
                array('text' => '+5', 'callback_data' => "+5"),
                array('text' => '+10', 'callback_data' => "+10"),
                array('text' => '+15', 'callback_data' => "+15"),
            )
        );



        $article = new Article();
        $article->id = uniqid();
        $article->title = 'Show top 15';
        $article->input_message_content = $text;
        $article->reply_markup = $markup;

        $answerQuery = new AnswerInlineQuery();
        $answerQuery->inline_query_id = $this->getQuery()->id;
        $answerQuery->addResult($article);
        $answerQuery->cache_time = 5; // Время кеширования 5 секунд, это для теста

        $this->getTgLog()->performApiRequest($answerQuery);

    }

}