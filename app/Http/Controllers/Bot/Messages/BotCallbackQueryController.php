<?php

namespace App\Http\Controllers\Bot\Messages;


use App\Http\Controllers\Bot\User\UserController;
use unreal4u\TelegramAPI\Telegram\Types\CallbackQuery;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\Message;
use unreal4u\TelegramAPI\TgLog;

/**
 * Часть бота, знающая логику работы с сообщениями пришедшими в ответ на inlineKeyboard
 * Т.е. бот работает с inlineKeyboard
 * Class BotCallBackQuery
 * @package app\Http\Controllers
 */
class BotCallbackQueryController extends BotDataController
{

    /** Список комманд */
    const THANKS_CMD = '/thx';
    const HIDE_CMD = '/hide';
    const PUNISH_CMD = '/punish';
    const INFO_CMD = '/info';

    /** @var  Message сообщение, к котому прикреплена inlineKeyboard*/
    private $message;

    /**
     * BotCallbackQueryController constructor.
     * @param TgLog $tgLog
     * @param CallbackQuery $query
     */
    public function __construct(TgLog $tgLog, CallbackQuery $query)
    {
        // вызываем первым делом родителя он инициализует основные вещи для дальнейшей работы
        parent::__construct($tgLog);
        // Инизиализируем сообщение
        $this->setMessage($query->message);
        // Инициализация чата
        $this->setChat($this->getMessage()->chat);
        // Инициализируем пользовалея, от которого пришла активность (нажатие на кнопку)
        $this->setFromUserController(new UserController($query->from->id, $this->getChat()->id, $query->from));
        // Инициализируем самого бота
        $this->initBotFromBD($this->getChat()->id);
        // Инициализируем callback, то пришло от нажатия на кнопку
        $this->setDataText($query->data);
        // Задаем ID callbackQuery
        $this->setDataId($query->id);
    }


    public function run()
    {
        // Сохраняем сообщенеи (callback) в БД
        $this->getUserMessageController()->addMessage($this);

        $command = $this->getTools()->getCommandFromMessageText($this->getDataText());

        switch ($command) {
            case self::THANKS_CMD :
                $this->thanks();
                break;
            case self::HIDE_CMD :
                $this->hideText();
                break;
            case self::PUNISH_CMD :
                $this->punish();
                break;
            case self::INFO_CMD :
                $this->showInfo();
                break;

            default:
                // TODO будет ли обработка чего-то другого?
                break;
        }
    }

    /**
     * Задаем тип объект
     */
    protected function setMyType()
    {
        $this->setType(BotDataController::CALLBACK_QUERY_TYPE);
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @param Message $message
     */
    public function setMessage(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Обработка пришедшего запроса по команде /thx и решаем что запускать
     */
    private function thanks()
    {
        // Если пытается ответить другой пользователь - то мы его обламываем , ну и попутно сообщенеи посылаем
        // С одной стороны можно засунуть в run(), но вдруг будут общие обработки, например типа голосования и т.д. если не будет, то вынесу в run()
        if ($this->isWrongUserAnswer()) {
            return false;
        }

        // Убираем из callback команду, и получаем число параметры для дальнейшего парсинга
        $query = trim(str_replace(self::THANKS_CMD, '', $this->getDataText()));

        // TODO сделать обратоку команды с логином бота:  /showtop@MrGnumBot5 - например
        $pattern = '/^(?P<count>[\d]+)u(?P<userId>[\d]+)/';
        preg_match($pattern, $query, $matches);
        // Если мы нашли количество на сколько увеличит очки и ID юзера, то вызываем thanksUser
        if (isset($matches['count']) && isset($matches['userId'])) {
            return $this->thanksUser($matches['count'], $matches['userId']);
        }

        $pattern = '/^u(?P<userId>[\d]+)/';
        preg_match($pattern, $query, $matches);
        // Если мы поняли, что сразу пришел userId, а не количество + userId, то значит был выбор пользователя
        // и теперь надо спросить, как его поблагодарить
        if (isset($matches['userId'])) {
            return $this->confirmThanksUser($matches['userId']);
        }

        return false;
    }


    /**
     * Обработка благодарности пользователя
     * @param int $count
     * @param int $userId
     * @return bool
     */
    private function thanksUser(int $count, int $userId)
    {
        $editMessage = $this->createEditedMessage();

        // TODO тут есть шанс, что создастся объект где getUser() - будет возвращать null и тогда вылетит Exception
        $userToThanks = new UserController($userId, $this->getChat()->id);
        // Если что-то нашли и количество не равно 0, прибавляем пользователю очки
        if ($count != 0) {

            $userToThanks->incrementPoints($count);

            $editMessage->text = "{$this->getFromUserController()->getFullName()} успешно поблагодарил(а) {$userToThanks->getFullName()}, добавив <b>{$count} очков!</b> Всего: <b>{$userToThanks->getUser()->points}</b>";
            // Спрятать inline Keyboard
            $editMessage->reply_markup = new Markup();

            $this->getTgLog()->performApiRequest($editMessage);
        } else {
            // Значит пишем, что никто благодарить не собирается и убираем inline Keyboard
            $editMessage->text = "Печально, {$this->getFromUserController()->getFullName()} не захотел(а) поблагодарить {$userToThanks->getFullName()} 😔";
            // Спрятать inline Keyboard
            $editMessage->reply_markup = new Markup();

            $this->getTgLog()->performApiRequest($editMessage);
            
        }

        return true;
    }

    /**
     * Предоставляем выбор на сколкьо отбалгодарить и точно ли хотят отблагодарить
     * @param int $userId
     * @return bool
     */
    private function confirmThanksUser(int $userId)
    {
        $userTo = new UserController($userId, $this->getChat()->id);
        // Отправляем сообщение с подтверждением кого поблагодарить, тип сообщения EditMessage
        return $this->sendConfirmationToThanks($this->getFromUserController(), $userTo, BotDataController::EDIT_MESSAGE_TYPE);
    }


    /**
     * Пытаемся скрыть сообщение
     */
    private function hideText()
    {
        // Если пытается ответить другой пользователь - то мы его обламываем , ну и попутно сообщенеи посылаем
        // С одной стороны можно засунуть в run(), но вдруг будут общие обработки, например типа голосования и т.д. если не будет, то вынесу в run()
        if ($this->isWrongUserAnswer()) {
            return false;
        }

        $editMessage = $this->createEditedMessage();
        $editMessage->text = '<i>Deleted</i>';
        // Спрятать inline Keyboard
        $editMessage->reply_markup = new Markup();

        $this->getTgLog()->performApiRequest($editMessage);
    }

    /**
     * Обработка наказания пользователя
     * @return bool
     */
    private function punish()
    {
        // Если пытается ответить другой пользователь - то мы его обламываем , ну и попутно сообщенеи посылаем
        // С одной стороны можно засунуть в run(), но вдруг будут общие обработки, например типа голосования и т.д. если не будет, то вынесу в run()
        if ($this->isWrongUserAnswer()) {
            return false;
        }

        // Убираем из callback команду, и получаем число параметры для дальнейшего парсинга
        $query = trim(str_replace(self::PUNISH_CMD, '', $this->getDataText()));

        // TODO сделать обратоку команды с логином бота:  /showtop@MrGnumBot5 - например
        $pattern = '/^(?P<count>[\d]+)u(?P<userId>[\d]+)/';
        preg_match($pattern, $query, $matches);
        // Если мы нашли количество на сколько уменьшить очки и ID юзера, то вызываем punishUser
        if (isset($matches['count']) && isset($matches['userId'])) {
            return $this->punishUser($matches['count'], $matches['userId']);
        }

        $pattern = '/^u(?P<userId>[\d]+)/';
        preg_match($pattern, $query, $matches);
        // Если мы поняли, что сразу пришел userId, а не количество + userId, то значит был выбор пользователя
        // и теперь надо спросить, как его наказать
        if (isset($matches['userId'])) {
            return $this->confirmPunishUser($matches['userId']);
        }
    }


    /**
     * Обработка наказания ползователя
     * @param int $count
     * @param int $userId
     * @return bool
     */
    private function punishUser(int $count, int $userId)
    {
        $editMessage = $this->createEditedMessage();
        // TODO тут есть шанс, что создастся объект где getUser() - будет возвращать null и тогда вылетит Exception
        $userToPunish = new UserController($userId, $this->getChat()->id);

        // Если что-то нашли и количество не равно 0, прибавляем пользователю очки
        if ($count != 0) {
            $userToPunish->decrementPoints($count);

            $editMessage->text = "{$this->getFromUserController()->getFullName()} наказал(а) {$userToPunish->getFullName()}, отняв <b>{$count} очков!</b> Всего: <b>{$userToPunish->getUser()->points}</b>";
            // Спрятать inline Keyboard
            $editMessage->reply_markup = new Markup();

        } else {
            // Значит пишем, что наказывать передумали и убираем inline Keyboard

            $editMessage->text = "Ура! {$userToPunish->getFullName()}, возрадуйся! {$this->getFromUserController()->getFullName()} передумал наказывать тебя!" . PHP_EOL;
            $editMessage->text .= 'P.s А ведь мог бы 😈';
            // Спрятать inline Keyboard
            $editMessage->reply_markup = new Markup();

        }

        $this->getTgLog()->performApiRequest($editMessage);
        return true;
    }

    /**
     * Отправялем запрос на сколкьо хотят наказать и хотят ли вообще наказать
     * @param int $userId
     * @return bool
     */
    private function confirmPunishUser(int $userId)
    {
        $userTo = new UserController($userId, $this->getChat()->id);
        // Отправляем сообщение с подтверждением кого наказать, тип сообщения EditMessage
        return $this->sendConfirmationToPunish($this->getFromUserController(), $userTo, BotDataController::EDIT_MESSAGE_TYPE);
    }


    /**
     * Проверяем коррекный ли пользователь нажимает на inlineKeyboard.
     * Т.к. мы отвечаем reply на сообщение, то становится ясно, кто должен отвечать исходя из этого
     * мы и пытаемся определить и если люди не совпадают, то шлем оповещение и игнорим дальнейшую логику
     * @return bool
     */
    private function isWrongUserAnswer()
    {
        // Если это сообщение является ответом на какое-то сообщение, то только в этом случае мы сможем понять кому предназначался ответ
        if (!is_null($this->getMessage()->reply_to_message)) {
            $replyMessage = $this->getMessage()->reply_to_message;
            // Получаем юзера, кому отвечали
            $userTo = new UserController($replyMessage->from->id, $this->getChat()->id);
            // Сравниваем и если они не совпадают, то шлем алерт, что а-я-яй, сообщение не для тебя
            if ($userTo->getUser()->id != $this->getFromUserController()->getUser()->id) {

                $text = "Данное сообщение предназначается для {$userTo->getFullName()}, а не вам!". PHP_EOL;
                $text .= "{$this->getFromUserController()->getFullName()}, будьте пожалуйста, внимательны!";

                // return true, Значит выдали ошибку, и больше не реагируем
                return $this->sendAlert($this->getDataId(), $text, true);
            }
        }

        // Возвращаем false - значит все окей, отвечает тот, кто нужен или же проверка не возможна, значит тоже окей
        return false;
    }



    /**
     * Отображаем информацию о выбранном пользователе
     * @return bool
     */
    private function showInfo()
    {
        // Если пытается ответить другой пользователь - то мы его обламываем , ну и попутно сообщенеи посылаем
        // С одной стороны можно засунуть в run(), но вдруг будут общие обработки, например типа голосования и т.д. если не будет, то вынесу в run()
        if ($this->isWrongUserAnswer()) {
            return false;
        }

        // Убираем из callback команду, и получаем число параметры для дальнейшего парсинга
        $query = trim(str_replace(self::INFO_CMD, '', $this->getDataText()));

        $pattern = '/^u(?P<userId>[\d]+)/';
        preg_match($pattern, $query, $matches);
        // Если мы поняли, что сразу пришел userId, а не количество + userId, то значит был выбор пользователя
        // и теперь надо спросить, как его наказать
        if (isset($matches['userId'])) {
            $user = new UserController($matches['userId'], $this->getChat()->id);

            return $this->sendUserInfo($user, BotDataController::EDIT_MESSAGE_TYPE);
        }

        return false;
    }
}