<?php

namespace App\Http\Controllers\Bot\Messages;

use App\Http\Controllers\Bot\AntiSpam\AntiSpamController;
use App\Http\Controllers\Bot\Games\BullsAndCowsController;
use App\Http\Controllers\Bot\User\UserController;
use App\User;


/**
 * Часть бота, знающая как работать с обычнми сообщениями и как на них реагировать
 * Class BotMessageController
 * @package app\Http\Controllers
 */
class BotMessageController extends BotGeneralMessageController
{

    // Основные команлы бота
    const HELP_CMD = '/help';
    const HELP_THX_PUNISH_CMD = '/helpthxpunish';
    const SHOW_TOP_CMD = '/showtop';
    const SHOW_MY_POINTS_CMD = '/showmypoints';
    const SHOW_MY_PLACE_CMD = '/showmyplace';
    const THANKS_CMD = '/thx';
    const PUNISH_CMD = '/punish';
    const INFO_CMD = '/info';
    const WHATS_NEW_CMD = '/whatsnew';
    const BULLS_CMD = BullsAndCowsController::BULLS_CMD;

    /**
     * основной метод для запуска
     */
    public function run()
    {
        // Сохраняем сообщение в БД
        $this->getUserMessageController()->addMessage($this);
        // Проверяем на спам
        $antiSpam = new AntiSpamController($this);
        // Если run - венёт false, значит был спам и мы ничего больше не делаем, он сам все сделал уже
        if (!$antiSpam->run()) {
            return false;
        }

        \Debugbar::addMessage($this->getMessage()->text, 'MessageText');
        $command = $this->getTools()->getCommandFromMessageText($this->getMessage()->text);
        \Debugbar::addMessage($command,'command');
        // Проверяем основыне каманды
        switch ($command) {
            case self::HELP_CMD :
                $this->showHelp();
                break;
            case self::HELP_THX_PUNISH_CMD :
                $this->showHelpThxPunish();
                break;
            case self::SHOW_TOP_CMD :
                $this->showTop();
                break;
            case self::SHOW_MY_POINTS_CMD :
                $this->showMyPoints();
                break;
            case self::SHOW_MY_PLACE_CMD :
                $this->showMyPlace();
                break;
            case self::THANKS_CMD :
                $this->sayThanks();
                break;
            case self::PUNISH_CMD :
                $this->punish();
                break;
            case self::INFO_CMD :
                $this->showUserInfo();
                break;
            case self::WHATS_NEW_CMD :
                $this->whatIsNew();
                break;
            case self::BULLS_CMD :
                BullsAndCowsController::getInstance($this)->run();
                break;
            default:
                // TODO обработка не стандартных случаев. Будут тут? или же отдельно?
                break;
        }
    }


    /**
     * Задаем тип объект
     */
    protected function setMyType()
    {
        $this->setType(BotDataController::SEND_MESSAGE_TYPE);
    }

    /**
     * Выводит в чат помощь
     */
    protected function showHelp()
    {
        $sendMessage = $this->createSendMessage();
        $sendMessage->text = 'Вас привествует развлекательный бот @MrGnumBot' . PHP_EOL . PHP_EOL;
        $sendMessage->text .= 'Список команд:' . PHP_EOL;
        $sendMessage->text .= self::HELP_CMD . ' - Вызвать помощь' . PHP_EOL;
        $sendMessage->text .= self::HELP_THX_PUNISH_CMD . ' - Пояснение о работе функционала поблагодарить/наказать (<b>Warning!</b> много текста)' . PHP_EOL;
        $sendMessage->text .= self::SHOW_TOP_CMD . ' - Показывает топ 10 пользователй по очкам, можно передать параметр от 0 до '. BotDataController::MAX_SHOW_TOP_COUNT .', например /showtop5' . PHP_EOL;
        $sendMessage->text .= self::SHOW_MY_POINTS_CMD . ' - Показывает ваше текущее количество очков' . PHP_EOL;
        $sendMessage->text .= self::SHOW_MY_PLACE_CMD . ' - Показывает ваше текущее место среди всех игроков' . PHP_EOL;
        $sendMessage->text .= self::INFO_CMD . ' - Показывает небольшую сводку. Можно смотреть как себя, так и других.' . PHP_EOL;
        $sendMessage->text .= self::WHATS_NEW_CMD . ' - Показывает, что нового было в последнем обновлении.' . PHP_EOL;
        $sendMessage->text .= BullsAndCowsController::BULLS_CMD . ' - Игра "Быки и Коровы"' . PHP_EOL;

        $this->getTgLog()->performApiRequest($sendMessage);
    }


    /**
     * Выводит информацию "Что нового"
     */
    protected function whatIsNew()
    {
        $sendMessage = $this->createSendMessage();
        $sendMessage->text = 'Что нового:'.PHP_EOL;
        $sendMessage->text .= '- Поправил антифлуд для help, а так же введен антифлуд на ' . self::THANKS_CMD . ' и ' . self::PUNISH_CMD  . PHP_EOL;

        $this->getTgLog()->performApiRequest($sendMessage);
    }

    /**
     * Выводит подробное описанеи функицонала поблагодарить/наказать
     */
    protected function showHelpThxPunish()
    {
        $sendMessage = $this->createSendMessage();
        $sendMessage->text = 'Подробное описание функционала "Поблагодарить" / "Наказать"' . PHP_EOL . PHP_EOL;
        $sendMessage->text .= 'Для того, что бы поблагодарить или наказать, необходимо использовать следующие команды: ' . self::THANKS_CMD . ' или ' . self::PUNISH_CMD . ' cоответственно.' . PHP_EOL;
        $sendMessage->text .= 'Использовать эти команды можно несколькими способами:' . PHP_EOL . PHP_EOL;
        $sendMessage->text .= '1) Сделать Reply и в сообщении написаь команду ' . self::THANKS_CMD . ' или ' . self::PUNISH_CMD . '. В таком случае каманда применится к тому, на чьё сообщение вы ответили.' . PHP_EOL . PHP_EOL;
        $sendMessage->text .= '2) Написать команду и после неё логин пользователя, например: ' . self::THANKS_CMD . '@MrGnumBot и вам будет предложен вариант кого поблагодарить. С наказанием аналогично. При написании символа @, вам автоматом будут предложены варианты логинов из чата (если кто не знал 😉)' . PHP_EOL . PHP_EOL;
        $sendMessage->text .= '3) Написать команду и после неё Имя или Фамилию или Логин (без символа @ ). Например:  ' . self::THANKS_CMD . ' Иван , и будет осуществлен поиск к кому вы хотели применить команду.' . PHP_EOL;
        $sendMessage->text .= 'Будет осуществляться нестрогий поиск по полям Имя, Фамилия и Логин и если что-то нашлось - вам будет предложено выбрать.' . PHP_EOL . PHP_EOL;
        $sendMessage->text .= 'P.s. во избежание спама, на команды без параметров (' . self::THANKS_CMD .' или ' . self::PUNISH_CMD . ') или же с параметром из 1 символа (' . self::PUNISH_CMD . ' И) - Реакции не будет. (Возможно обсуждение как лучше сделать)';
        $this->getTgLog()->performApiRequest($sendMessage);
    }
    /**
     * Выводит в чат топ по очкам
     * @param int $topCount
     */
    protected function showTop(int $topCount = 10)
    {
        // Если пришел параметр после команлы и это число - обрабатываем его
        // TODO сделать обратоку команды с логином бота:  /showtop@MrGnumBot5 или /showtop@MrGnumBot 5  - например
        // вид регулярки, для лучшего понимания /^\/showtop[\s]*(?P<count>[\d]+)/
        $pattern = "/^\\" . self::SHOW_TOP_CMD .'[\s]*(?P<count>[\d]+)/';
        $maxTopCount = BotDataController::MAX_SHOW_TOP_COUNT;
        preg_match($pattern, $this->getMessage()->text, $matches);
        if (!empty($matches)) {
            $count = intval($matches['count']);
            // Если пришло число в диапазоне от 0 до $maxTopCount, то показываем такое количество человек
            if ($count > 0 && $count <= $maxTopCount) {
                $topCount = $count;
            // Если больше максимума - показываем максимум
            } elseif($count > $maxTopCount) {
                $topCount = $maxTopCount;
            }
        }

        // Получаем первых $topCount пользователй, по очкам
        $users = User::select(array('telegram_id'))
            ->whereTelegramChatId($this->getChat()->id)
            ->orderBy('points', 'desc')
            ->limit($topCount)
            ->get();

        $sendMessage = $this->createSendMessage();
        $sendMessage->text = "Лучшие $topCount игроков:" . PHP_EOL;
        $prevPointCount = 0;
        $number = 1;
        foreach ($users->all() as $key => $user ) {
            /** @var User $user */
            $userController = new UserController($user->telegram_id, $this->getChat()->id);

            // Если количесто очков совпадает с предыдущим участником, то место не повышаем, а оставляем предыдущее
            // Таким образом может быть два первых места, а затем сразу же 3е место, 2ого в таком случае не будет
            // key == 0, что бы на первой итерации это правило игнорилось )
            if ($key == 0 || $prevPointCount != $userController->getUser()->points) {
                // Т.к. массивы нумеруются с нуля
                $number = $key + 1;
            }
            // Формуируем строки вида: "1. Петр Петров (@petrov) - 3424"
            $sendMessage->text .= "{$number}. {$userController->getFullName(false)} \t <b>{$userController->getUser()->points}</b>" . PHP_EOL;
            $prevPointCount = $userController->getUser()->points;
        }

        $this->getTgLog()->performApiRequest($sendMessage);
    }


    /**
     * Выводит в чат количество очков пользователя, котоырй сделал запрос
     */
    protected function showMyPoints()
    {
        $sendMessage = $this->createSendMessage();

        $sendMessage->text = 'Ваше текущее количество очков: <b>' . $this->getFromUserController()->getUser()->points . '</b>';
        $this->getTgLog()->performApiRequest($sendMessage);
    }


    /**
     * Выводит в чат место пользвоателя, среди всех участников чата
     */
    protected function showMyPlace()
    {
        $count =  $this->getUserPlace($this->getFromUserController());

        $sendMessage = $this->createSendMessage();

        if ($count == 1) {
            $sendMessage->text = 'Поздравляю! ';
        }

        $sendMessage->text .= "Вы занимаете <b>$count-ое место</b>, среди всех игроков";
        $this->getTgLog()->performApiRequest($sendMessage);
    }


    /**
     * Сказать спасибо.
     * Отправляет в чат сообщение с inlineKeyboard, где предлагает отблагодарить человека
     */
    private function sayThanks()
    {

        // Если это сообщение не является reply на другое сообщение, пытаемся найти, кого же хотели поблагодарить
        if (is_null($this->getMessage()->reply_to_message)) {
            // пытаемся получить, передаем строку, отсеив команду
            $users = $this->findUser(str_replace(self::THANKS_CMD, '', $this->getMessage()->text));
            // Если что-то нашли, и это несколько человек, то формируем сообщение с тем, что пытаемся выяснить кого благодарить
            if (count($users) > 1) {
                return $this->SendChooseWhoYouWantToThank($users);

            // Если мы нашли только одного, значит именно его и надо поблагодарить - капитан =)
            } elseif (count($users) == 1) {
                // Берем первое значение, т.к. оно там единственное
                $user = reset($users);
                // и выставялем
                $userTo = new UserController($user->telegram_id, $this->getChat()->id);
                return $this->sendConfirmationToThanks($this->getFromUserController(), $userTo);

            // Если это не reply и при этом мы не смогли найти кого благодарить, то ничего не делаем
            } else {
                // TODO сделать отправку Alert о неудачном поиске (такую не навязчивую плажку отправлять
                return false;
            }
        } else {
            // Отправляем запрос на подтверждение кого поблагодарить
            return $this->sendConfirmationToThanks($this->getFromUserController(), $this->getReplyUserController());
        }
    }


    /**
     * Наказать пользоваетля, снять какое-то количество очков
     */
    private function punish()
    {

        // Если это сообщение не является reply на другое сообщение
        if (is_null($this->getMessage()->reply_to_message)) {
            // пытаемся получить из параметров, кого наказать, передаем строку, отсеив команду
            $users = $this->findUser(str_replace(self::PUNISH_CMD, '', $this->getMessage()->text));

            // Если что-то нашли, и это несколько человек, то формируем сообщение с тем, что пытаемся выяснить кого наказать
            if (count($users) > 1) {
                return $this->SendChooseWhoYouWantToPunish($users);

                // Если мы нашли только одного, значит именно его и надо наказать - капитан =)
            } elseif (count($users) == 1) {
                // Берем первое значение, т.к. оно там единственное
                $user = reset($users);
                // и получаем юзера
                $userTo = new UserController($user->telegram_id, $this->getChat()->id);
                return $this->sendConfirmationToPunish($this->getFromUserController(), $userTo);

                // Если это не reply и при этом мы не смогли найти кого наказать, то ничего не делаем
            } else {
                // TODO сделать отправку Alert о неудачном поиске (такую не навязчивую плажку отправлять
                return false;
            }

        } else {
            // Отправялем запрос на подтверждение, кого наказать
            return $this->sendConfirmationToPunish($this->getFromUserController(), $this->getReplyUserController());
        }
    }


    /**
     * Показывает сводку по пользователю
     */
    private function showUserInfo()
    {

        // Если это сообщение не является reply на другое сообщение
        if (is_null($this->getMessage()->reply_to_message)) {
            // готовим строку, вырезаем команду и обрезаем лишние пробелы
            $string = trim(str_replace(self::INFO_CMD, '', $this->getMessage()->text));
            // Если после этого длинна строки стала 0, значит это прсото была команда /info и мы отправляем инфу о запросившем пользователе
            if (mb_strlen($string) == 0) {
                return $this->sendUserInfo($this->getFromUserController());
            }
            // Если же это не так, значит там есть параметры, то мы пытаемся получить из параметров, чью инофрмацию необходимо отобразить
            $users = $this->findUser($string);

            // Если что-то нашли, и это несколько человек, то формируем сообщение с тем, что пытаемся выяснить чью инфу показывать?
            if (count($users) > 1) {
                return $this->SendChooseWhoseInfoShow($users);

            // Если мы нашли только одного, значит именно его информацию и надо показать
            } elseif (count($users) == 1) {
                // Берем первое значение, т.к. оно там единственное
                $user = reset($users);
                // и получаем юзера
                $userTo = new UserController($user->telegram_id, $this->getChat()->id);
                return $this->sendUserInfo($userTo);

            // Если это не reply и при этом мы не смогли найти чью инфу показываем
            } else {
                // TODO сделать отправку Alert о неудачном поиске (такую не навязчивую плажку отправлять
                return false;
            }
        } else {
            // Раз это reply - отправляем инфу о том, кому делали reply
            return $this->sendUserInfo($this->getReplyUserController());
        }




    }
}