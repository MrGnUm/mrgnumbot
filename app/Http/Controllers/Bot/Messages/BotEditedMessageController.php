<?php

namespace App\Http\Controllers\Bot\Messages;


/**
 * Часть бота, отвечающая за работу с Измененными сообщениями, частино их логику будет пересекаться с обычными сообщениями
 * Class BotEditedMessageController
 * @package app\Http\Controllers
 */
class BotEditedMessageController extends BotGeneralMessageController
{

    public function run()
    {
        \Debugbar::addMessage('Run from BotEditedMessageController');
    }

    /**
     * Задаем тип объект
     */
    protected function setMyType()
    {
        $this->setType(BotDataController::EDIT_MESSAGE_TYPE);
    }
}