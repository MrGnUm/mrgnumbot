<?php

namespace App\Http\Controllers\Bot\Messages;


use App\Http\Controllers\Bot\User\UserController;
use App\UserMessage;
use unreal4u\TelegramAPI\Telegram\Types\Message;
use unreal4u\TelegramAPI\TgLog;

/**
 * Общий класс для сообщенйи типа Message и EditedMessage - основаня логика и структура у них схожи,
 * но т.к. могут быть отличия в логике обработки данных, то есть потомки каждого из этих типов
 * Class BotGeneralMessageController
 * @package app\Http\Controllers
 */
abstract class BotGeneralMessageController extends BotDataController
{

    /** @var  Message */
    private $message;
    /** @var  Message */
    private $replyMessage;
    /** @var  UserController */
    private $replyUserController;

    /**
     * BotGeneralMessageController constructor.
     * @param TgLog $tgLog
     * @param Message $message
     */
    public function __construct(TgLog $tgLog, Message $message)
    {
        // вызываем первым делом родителя он инициализует основные вещи для дальнейшей работы
        parent::__construct($tgLog);

        // Инициализируем объект сообщение
        $this->setMessage($message);
        // Сохраняем пришедший текст
        $this->setDataText($message->text);
        // Инициализируем ID пришедшего запроса (в данном случае ID сообщения)
        $this->setDataId($message->message_id);
        // Инициализиурем объект чата
        $this->setChat($this->getMessage()->chat);
        // Это сообщение является ответом (reply )  то дополнительно определяем объекты
        if (!is_null($message->reply_to_message)) {
            // Инициализируем сообщение, на которое отвечали
            $this->setReplyMessage($message->reply_to_message);
            // Инициализируем пользователя, которому ответили
            $this->setReplyUserController(new UserController($this->getReplyMessage()->from->id, $this->getChat()->id, $this->getReplyMessage()->from));
        }

        // Инициализируем самого бота
        $this->initBotFromBD($this->getChat()->id);

        // Получаем объект пользователя по его id из телеграма + id чата
        $this->setFromUserController(new UserController($this->getMessage()->from->id, $this->getChat()->id, $this->getMessage()->from));

    }


    /**
     * Сохраним пришедшее сообщение от пользователя в БД
     * @param string $type - тип сообщения
     * @return bool
     */
    protected function saveUserMessage(string  $type)
    {
        $message = $this->getMessage();
        $userMessageModel = new UserMessage();
        $userMessageModel->message_id = $message->message_id;
        $userMessageModel->chat_id = $this->getChat()->id;
        $userMessageModel->telegram_user_id = $this->getFromUserController()->getUser()->telegram_id;
        $userMessageModel->send_date = $message->date;
        $userMessageModel->edit_date = $message->edit_date;
        $userMessageModel->text = $message->text;
        $userMessageModel->message_type = $type;

        $userMessageModel->save();

        return true;
    }


    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @param Message $message
     */
    public function setMessage(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @return Message
     */
    public function getReplyMessage(): Message
    {
        return $this->replyMessage;
    }

    /**
     * @param Message $replyMessage
     */
    public function setReplyMessage(Message $replyMessage)
    {
        $this->replyMessage = $replyMessage;
    }

    /**
     * @return UserController
     */
    public function getReplyUserController(): UserController
    {
        return $this->replyUserController;
    }

    /**
     * @param UserController $replyUserController
     */
    public function setReplyUserController(UserController $replyUserController)
    {
        $this->replyUserController = $replyUserController;
    }

}