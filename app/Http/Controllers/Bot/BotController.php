<?php

namespace App\Http\Controllers\Bot;


use App\BotUpdate;
use App\Http\Controllers\Bot\Messages\BotCallbackQueryController;
use App\Http\Controllers\Bot\Messages\BotDataController;
use App\Http\Controllers\Bot\Messages\BotEditedMessageController;
use App\Http\Controllers\Bot\Messages\BotInlineQueryController;
use App\Http\Controllers\Bot\Messages\BotMessageController;
use App\Http\Controllers\Controller;
use unreal4u\TelegramAPI\Telegram\Methods\GetUpdates;
use unreal4u\TelegramAPI\Telegram\Types\Custom\UpdatesArray;
use unreal4u\TelegramAPI\Telegram\Types\Update;
use unreal4u\TelegramAPI\TgLog;


/**
 * Главный класс, управление всей основной логикой бота
 * Основной метод run
 * Class BotController
 * @package App\Http\Controllers
 */
class BotController extends Controller
{


    /** @var  string */
    private $botToken;
    /** @var  TgLog основной объект, который работает с API Telegram */
    private $tgLog;
    /** @var  BotDataController */
    private $botDataController;


    /**
     * BotController constructor.
     */
    public function __construct()
    {
        if (empty(env('TELEGRAM_BOT_TOKEN'))) {
            throw new \Exception('Telegram Bot Token is empty!');
        }
        $this->setBotToken(env('TELEGRAM_BOT_TOKEN'));
        // Создаем главный объект для работы с API Telegram
        $this->setTgLog(new TgLog($this->getBotToken()));
    }


    /**
     * основной метод, который
     * @param Update $update
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run(Update $update)
    {
        if ($this->initBot($update)) {
            // Запускаем логику работы с тем типом данных, что пришли в initBot() - они корректно распределились
            $this->getBotDataController()->run();
        }
        return view('bot');
    }


    /**
     * Получаем данные через WebHook
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function runFromWebHook()
    {
        // Получаем объект update
        $update = new Update(\Input::all());
        return $this->run($update);
    }


    /**
     * Получаем данные с помощью getUpdates
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function runFromGetUpdates()
    {

        $lastUpdateId = BotUpdate::orderBy('id', 'desc')->first();
        $getUpdate = new GetUpdates();
        if (!is_null($lastUpdateId)) {
            // Запрашиваем следующее сообщение, т.е. после посленего чтоу  нас было + 1
            $getUpdate->offset = $lastUpdateId->update_id +1;
        }
        /** @var UpdatesArray $updates */
        $updates = $this->getTgLog()->performApiRequest($getUpdate);
        /** @var Update $lastUpdate */
        $lastUpdate = end($updates->data);
        \Debugbar::addMessage($lastUpdate, 'lastUpdate');
        // Если ничего не пришло - возвращаем false
        // Сохраняем в Бд значение последнего update если значения отличаются
        if (!empty($lastUpdate) && !empty($lastUpdateId) && $lastUpdateId->update_id != $lastUpdate->update_id) {
            // TODO временно закмментировал для теста
            $newLastUpdateId = new BotUpdate();
            $newLastUpdateId->update_id = $lastUpdate->update_id;
            $newLastUpdateId->save();
        }


        // Если в итоге пусто, то создаем пустой объект и пытаемся его передать
        if (empty($lastUpdate)) {
            $lastUpdate = new Update();
        }

        // TODO возможно стоит сделать обработку каждого сообщения в update  - для корреткной работы через getUpdate а не только последнего
        return $this->run($lastUpdate);
    }

    /**
     * Инициализация бота
     * Достаем бота из БД и сохранем его данные, если бота нет - сохраняем бота как в БД так и в объект
     * @param Update $update
     * @return bool
     * @throws \Exception
     */
    protected function initBot(Update $update)
    {

        if (empty($update)) {
            // Елсли ничего не пришло - игнорим
            return false;
        }

        // Если пустое обычное сообщение, берем объект сообщения из edited_message и себе помечаем, что сообщение измененное
        if (!is_null($update->message)) {
            $this->setBotDataController(new BotMessageController($this->getTgLog(), $update->message));

        // Если это изменное сообщение
        } elseif (!is_null($update->edited_message)) {
            $this->setBotDataController(new BotEditedMessageController($this->getTgLog(), $update->edited_message));

        // Если это реакция на inlineKeyboard
        } elseif (!is_null($update->callback_query)) {
            $this->setBotDataController(new BotCallbackQueryController($this->getTgLog(), $update->callback_query));

        // Если это inlineQuery
        } elseif (!is_null($update->inline_query)) {
            $this->setBotDataController(new BotInlineQueryController($this->getTgLog(), $update->inline_query));
        // Если это то, чего мы еще не знаем =)
        } else {
            throw new \Exception('Неизвестный для бота, тип сообщения');

        }

        return true;
    }

    /**
     * @return BotDataController
     */
    private function getBotDataController(): BotDataController
    {
        return $this->botDataController;
    }

    /**
     * @param BotDataController $botDataController
     */
    private function setBotDataController(BotDataController $botDataController)
    {
        $this->botDataController = $botDataController;
    }

    /**
     * @return string
     */
    public function getBotToken(): string
    {
        return $this->botToken;
    }

    /**
     * @param string $botToken
     */
    public function setBotToken(string $botToken)
    {
        $this->botToken = $botToken;
    }

    /**
     * @return TgLog
     */
    public function getTgLog(): TgLog
    {
        return $this->tgLog;
    }

    /**
     * @param TgLog $tgLog
     */
    public function setTgLog(TgLog $tgLog)
    {
        $this->tgLog = $tgLog;
    }
}
