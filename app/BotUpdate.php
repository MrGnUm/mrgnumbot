<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BotUpdate extends Model
{

    protected $table = 'bot_updates';
}
