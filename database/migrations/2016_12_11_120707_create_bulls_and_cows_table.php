<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBullsAndCowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulls_ans_cows', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('chat_id');
            $table->string('question_number');
            $table->integer('attempts_count');
            $table->boolean('game_stop')->nullable();
            $table->bigInteger('winner_telegram_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bulls_ans_cows');
    }
}
