<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\BotUpdate
 *
 * @property integer $id
 * @property integer $update_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\BotUpdate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BotUpdate whereUpdateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BotUpdate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BotUpdate whereUpdatedAt($value)
 */
	class BotUpdate extends \Eloquent {}
}

namespace App{
/**
 * App\Task
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Task whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Task whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Task whereUpdatedAt($value)
 */
	class Task extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $telegram_login
 * @property integer $telegram_id
 * @property integer $telegram_chat_id
 * @property integer $points
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTelegramLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTelegramId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTelegramChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePoints($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\BullsAndCows
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $chat_id
 * @property string $question_number
 * @property integer $attempts_count
 * @property integer $winner_telegram_id
 * @property boolean $game_stop
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereQuestionNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereAttemptsCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereWinnerTelegramId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\BullsAndCows whereGameStop($value)
 */
	class BullsAndCows extends \Eloquent {}
}

namespace App{
/**
 * App\Bot
 *
 * @property integer $id
 * @property integer $telegram_chat_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Bot whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Bot whereTelegramChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Bot whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Bot whereUpdatedAt($value)
 */
	class Bot extends \Eloquent {}
}

namespace App{
/**
 * App\UserMessage
 *
 * @property integer $id
 * @property integer $message_id
 * @property integer $chat_id
 * @property integer $telegram_user_id
 * @property integer $send_date
 * @property integer $edit_date
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $message_type
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereMessageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereTelegramUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereSendDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereEditDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage whereMessageType($value)
 */
	class UserMessage extends \Eloquent {}
}

namespace App{
/**
 * App\Point
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Point whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Point whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Point whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Point whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Point whereUpdatedAt($value)
 */
	class Point extends \Eloquent {}
}

